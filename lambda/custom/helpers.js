const Xray = require('x-ray');

let Helpers = {};

Helpers.fetchTideData = async function() {
  const url = 'http://tides.mobilegeographics.com/locations/5538.html';
  const x = Xray();
  const predictions = await x(url, '.predictions-section', [{
    data: 'pre'
  }]);
  return predictions[0].data.split('\n').slice(3);
}

module.exports = Helpers;
