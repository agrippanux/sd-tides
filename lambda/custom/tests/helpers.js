const chai = require('chai');
const Helpers = require('../helpers');
const expect = chai.expect;
chai.config.includeStack = true;

describe('Helper', () => {

  describe('#fetchTideData', () => {
    it('returns an array', async () => {
      const tideData = await Helpers.fetchTideData();
      return expect(tideData).to.be.a('array');
    })
  });

	describe('#dummy', () => {
		context('not a real call', () => {
			it('marks as pending');
		});
	});
});
